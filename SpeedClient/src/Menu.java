import java.net.SocketException;
import java.util.Scanner;

public class Menu {

    Client client;

    Menu(Client client) {
        this.client = client;
    }

    public void start() {
        Scanner in = new Scanner(System.in);
        int switchInput;
        do {
            System.out.println("0 - End");
            System.out.println("1 - Start");
            System.out.println("2 - Nagle");
            System.out.println("3 - Buffer (default 100, min 50 max 65500)");
            System.out.println("4 - IP (default localhost)");
            System.out.println("5 - Port (default 7777)");
            switchInput = in.nextInt();
            switch (switchInput) {
                case 0:
                    if(client.running) {
                        System.out.println("Ending server session!");
                        client.end();
                    }
                    break;
                case 1:
                    if(!client.running) {
                        System.out.println("Starting server!");
                        client.create();
                    }
                    break;
                case 2:
                    if(!client.running) {
                        client.nagle();
                    }
                    else {
                        System.out.print("nagle ");
                        try {
                            if(client.tcp.clientSocket.getTcpNoDelay()) {
                                System.out.println("ON");
                            }
                            else {
                                System.out.println("OFF");
                            }
                        } catch (SocketException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case 3:
                    if(!client.running) {
                        System.out.println("Client buffer: " + client.bufferSize);
                        client.bufferSize = in.nextInt();
                        if (client.bufferSize < 50) {
                            client.bufferSize = 50;
                        }
                        if (client.bufferSize > 65500) {
                            client.bufferSize = 65500;
                        }
                        System.out.println("New client buffer: " + client.bufferSize);
                    } else {
                        System.out.println("Client is running! Stop client and change buffer!");
                    }
                    break;
                case 4:
                    if(!client.running) {
                        System.out.println("Client ip: " + client.addressIP);
                        client.addressIP = in.next();
                        System.out.println("New client ip: " + client.addressIP);
                    } else {
                        System.out.println("Client is running! Stop client and change buffer!");
                    }
                    break;
                case 5:
                    if(!client.running) {
                        System.out.println("Client port: " + client.port);
                        client.port = in.nextInt();
                        System.out.println("New client port: " + client.port);
                    } else {
                        System.out.println("Client is running! Stop client and change buffer!");
                    }
                    break;
            }
        } while(switchInput!=0);
    }
}
