import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;
import java.net.SocketException;
import java.net.UnknownHostException;

public class TCP extends Thread{
    Socket clientSocket;
    boolean nagle = false;
    int bufferSize = 100;
    boolean end = false;
    String ipAdress = "127.0.0.1";
    int port = 7777;

    TCP(boolean nagle, int bufferSize, String ipAdress, int port) {
        this.nagle = nagle;
        this.bufferSize = bufferSize;
        this.ipAdress = ipAdress;
        this.port = port;
    }

    public void run() {
        try {
            clientSocket = new Socket(ipAdress, port);
            clientSocket.setTcpNoDelay(nagle);
            DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
            sleep(200);
            byte[] firstMessage = new String("SIZE:" + bufferSize).getBytes();
            out.write(firstMessage, 0, firstMessage.length);
            byte[] buffer = initBuffer();
            while(!end) {
                sleep(10);
                out.write(buffer, 0, buffer.length);
            }
        } catch (SocketException e) {
			System.out.println("Socket closed! Program will exit...");
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			System.out.println("Unknown host! Program will exit...");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
    }

    public byte[] initBuffer() {
        byte[] buffer = new byte[bufferSize];
        Arrays.fill(buffer, (byte)bufferSize);
        return buffer;
    }

    public void end()
    {
        try {
            end = true;
            clientSocket.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
