public class Client {

    TCP tcp;
    UDP udp;
    boolean running = false;
    boolean nagle = false;
    int bufferSize = 100;
    String addressIP = "127.0.0.1";
    int port = 7777;

    public void create() {
        tcp = new TCP(nagle, bufferSize, addressIP, port);
        udp = new UDP(bufferSize, addressIP, port);
        tcp.start();
        udp.start();
        running = true;
    }

    public void end() {
        tcp.end();
        udp.end();
        running = false;
    }

    public void nagle() {
        nagle = !nagle;
    }
}
