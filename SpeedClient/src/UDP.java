import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UDP extends Thread {
    DatagramSocket server;
    int bufferSize = 100;
    boolean end = false;
    String addressIP = "127.0.0.1";
    int port = 7777;

    UDP(int bufferSize, String addressIP, int port) {
        this.bufferSize = bufferSize;
        this.addressIP = addressIP;
        this.port = port;
    }

    public void run()
    {
        byte[] buffer = initBuffer();
        try {
            server = new DatagramSocket();
            sleep(200);
            byte[] firstMessage = new String("SIZE:" + bufferSize).getBytes();
            DatagramPacket packet = new DatagramPacket(firstMessage, firstMessage.length, InetAddress.getByName(addressIP), port);
            server.send(packet);
            packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(addressIP), port);
            while (!end) {
                sleep(10);
                server.send(packet);
            }
        } catch (SocketException e) {
			System.out.println("Socket closed! Program will exit...");
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			System.out.println("Unknown host! Program will exit...");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
    }

    public byte[] initBuffer() {
        byte[] buf = new byte[bufferSize];
        Arrays.fill(buf, (byte)bufferSize);
        return buf;
    }

    public void end()
    {
        try {
            end = true;
            server.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
