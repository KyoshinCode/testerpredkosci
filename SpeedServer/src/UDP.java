import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.TimerTask;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UDP extends Thread {

    DatagramSocket server;
    boolean firstMessage = true;
    int bufferSize;
    long receivedData;
    long startTime;
    long currentTime;
    long transmissionTime;
    double transmissionSpeed;
    boolean end = false;
    int dataLength;
    int port = 7777;

    UDP(int port) {
        this.port = port;
    }

    public void run()
    {
        try {
            
            server = new DatagramSocket(port);
            byte[] messageByte = new byte[(int)65500.0D];
            
            while (!end) {
                DatagramPacket pack = new DatagramPacket(messageByte, messageByte.length);
                server.receive(pack);
                dataLength = pack.getLength();
                if (firstMessage) {
                    String[] firstMsg = new String(pack.getData(), pack.getOffset(), pack.getLength()).split(":");
                    if (firstMsg[0].equalsIgnoreCase("SIZE"))
                    {
                        bufferSize = new Integer(firstMsg[1]).intValue();
                        firstMessage = false;
                        startTime = System.currentTimeMillis();
                        receivedData = 0L;
                    }
                } else {
                    stats();
                }
            }
        } catch (SocketException e) {
			System.out.println("Socket closed! Program will exit...");
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			System.out.println("Unknown host! Program will exit...");
		} catch (IOException e) {
            end();
			System.out.println("Server closed!");
		} 
    }

    public void end()
    {
        end = true;
        server.close();
    }

    public void stats()
    {
        currentTime = System.currentTimeMillis();
        transmissionTime = (currentTime - startTime);
        receivedData += bufferSize;
        transmissionSpeed = (receivedData * 1000.0D / (transmissionTime));
    }

    public void resetStats() {
        firstMessage = true;
        startTime = 0;
        currentTime = 0;
        transmissionTime = 0;
        bufferSize = 0;
        receivedData = 0;
        transmissionSpeed = 0;
    }
}
