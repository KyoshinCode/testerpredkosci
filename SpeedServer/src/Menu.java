import java.util.Scanner;

public class Menu {

    int port = 7777;

    public void start() {
        Scanner in = new Scanner(System.in);
        TCP tcp = new TCP(port);
        UDP udp = new UDP(port);
        int switchInput;
        do {
            System.out.println("0 - End");
            System.out.println("1 - Start");
            System.out.println("2 - Statistic");
            System.out.println("3 - Statistic reset");
            System.out.println("4 - Port (default 7777)");
            switchInput = in.nextInt();
            switch (switchInput) {
                case 0:
                    tcp.end();
                    udp.end();
                    System.out.println("TCP:");
                    System.out.println("Single data size: " + tcp.bufferSize);
                    System.out.println("Received: " + tcp.receivedData);
                    System.out.println("Speed: " + tcp.transmissionSpeed);
                    System.out.println("Time: " + tcp.transmissionTime / 1000 + "s");
                    System.out.println("UDP:");
                    System.out.println("Single data size: " + udp.bufferSize);
                    System.out.println("Received: " + udp.receivedData);
                    System.out.println("Speed: " + udp.transmissionSpeed);
                    System.out.println("Time: " + udp.transmissionTime / 1000 + "s");
                    break;
                case 1:
                    tcp.start();
                    udp.start();
                    break;
                case 2:
                    System.out.println("TCP:");
                    System.out.println("Single data size: " + tcp.bufferSize);
                    System.out.println("Received: " + tcp.receivedData);
                    System.out.println("Speed: " + tcp.transmissionSpeed);
                    System.out.println("Time: " + tcp.transmissionTime / 1000 + "s");
                    System.out.println("UDP:");
                    System.out.println("Single data size: " + udp.bufferSize);
                    System.out.println("Received: " + udp.receivedData);
                    System.out.println("Speed: " + udp.transmissionSpeed);
                    System.out.println("Time: " + udp.transmissionTime / 1000 + "s");
                    break;
                case 3:
                    tcp.resetStatistics();
                    udp.resetStats();
                    break;
                case 4:
                    System.out.println("Server port: " + port);
                    port = in.nextInt();
                    System.out.println("New server port: " + port);
                    break;

            }
        } while(switchInput!=0);
    }

}
